const Sequelize = require('sequelize')
const db = require('../database/db.js')

module.exports = db.sequelize.define(
  'sociallinks',
  {
   
    email:{ type: Sequelize.STRING,
      primaryKey: true,
      autoIncrement: false},
    facebook: {
      type: Sequelize.STRING
    },
    twitter: {
      type: Sequelize.STRING
    },
    instagram: {
      type: Sequelize.STRING
    },
    weburl: {
      type: Sequelize.STRING
    },
    created: {
      type: Sequelize.DATE,
      defaultValue: Sequelize.NOW
    }
  },
  {
    timestamps: false
  }
)
