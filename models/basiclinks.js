const Sequelize = require('sequelize')
const db = require('../database/db.js')

module.exports = db.sequelize.define(
  'basiclinks',
  {
   appl_id:{type: Sequelize.STRING,
   

   },
    email:{ type: Sequelize.STRING,
      primaryKey: true,
      autoIncrement: false},
    
      Name: {
      type: Sequelize.STRING
    },
    companyName: {
      type: Sequelize.STRING
    },
    dial_code: {
      type: Sequelize.STRING
    },
    almamater: {
      type: Sequelize.STRING
    },
    experience: {
      type: Sequelize.STRING
    },
    awards: {
      type: Sequelize.STRING
    },
    designs: {
      type: Sequelize.STRING
    },
    establishment: {
      type: Sequelize.STRING
    },
    totaldesigners: {
      type: Sequelize.STRING
    },
    phonenumber: {
      type: Sequelize.STRING
    },
    foreignclients: {
      type: Sequelize.STRING
    },
    avgsales: {
      type: Sequelize.STRING
    },
    city:{
      type: Sequelize.STRING
    },
    Allocated:{
      type: Sequelize.BOOLEAN
    },
    Allocatedexecutiveid:{
      type:Sequelize.STRING
    },
    Allocatedexecutivename:{
      type:Sequelize.STRING
    },
   aadhar_card:{
     type:Sequelize.STRING
   },
   pan_card:{
     type:Sequelize.STRING
   },
   gst:{
     type:Sequelize.STRING
   },
   brd:{
     type:Sequelize.STRING
   }
  },
  {
    timestamps: false
  }
)
