'use strict'
const jwt = require('jsonwebtoken')
const env = require('../database/env.js');
const bcrypt = require('bcrypt')

const Basic = require('../models/basiclinks')
const nodemailer = require('nodemailer');

process.env.SECRET_KEY = 'secret';
var transporter = nodemailer.createTransport({
    service: env.service,
    auth: {
      user: env.email,
      pass:env.pass
    }
  });
var a = [1,2,3,4,5,6,7];


// var registration_num = arrange(a);



exports.plugin = {
  register: (server, options, next) => {
    server.route({
      method: 'POST',
      path: '/signup',
    
      handler: (req, h) => {
        function shuffle(a) {
          var j, x, i;
          for (i = a.length - 1; i > 0; i--) {
              j = Math.floor(Math.random() * (i + 1));
              x = a[i];
              a[i] = a[j];
              a[j] = x;
          }
          return a; 
        }
        var check = []
        var i,random="";
        
        function arrange(a){
          var c = shuffle(a);
        for (i = c.length - 1; i > 0; i--) {
        random+=c[i] +""
        }
        if(check.indexOf(random) !== -1){
          console.log("Value exists!")
          
          } else{
          console.log("Value does not exists!")
          check.push(random);
          } 
          return random;
        }

        const today = new Date()
        const userData = {
          
         appl_id:"celebref"+arrange(a),
          email: req.payload.email,
         
  Name: req.payload.Name,
  companyName: req.payload.companyName,
  dial_code: req.payload.dial_code,
  almamater: req.payload.almamater,
  awards: req.payload.awards,
  experience: req.payload.experience,
  designs: req.payload.designs,
  establishment: req.payload.establishment,
  totaldesigners: req.payload.totaldesigners,
  phonenumber: req.payload.phonenumber,
  foreignclients: req.payload.foreignclients,
  avgsales: req.payload.avgsales,
  city:req.payload.city,
  Allocated:false
          
        
        
        }
        console.log(userData);
        return Basic.findOne({
          where: {
            // 
            email: req.payload.email
          }
          
        })
        
          .then(user => {
            console.log(user+"abc");
            if (!user) {
             
                Basic.create(userData)
                  .then(res2 => {
                    var mailOptions = {
                        from: env.email,
                        to: req.payload.email,
                        subject: 'Welcome',
                       
                        html: '<h1>Dear '+   req.payload.Name+
                      '</h1><p>Thank you for your interest on working with us. Your registration number is <b> ' + userData.appl_id+ '</b>'+
                       ' .You will receive an update from our team shortly <br>' +
                        
                        'To track your application status please click here. <br>' +
                        
                       ' Thank you, <br>'+
                        
                      'Our Best,<br>'+
                       ' Celebramp Team.</p> '  
                      };
                      transporter.sendMail(mailOptions, function(error, info){
                        if (error) {
                          console.log(error);
                        } else {
                          console.log('Email sent: ' + info.response);
                        }
                      });
                    
                    console.log(res2+'aa');
                    return { status: res2.email + 'true' }
                  
                
              })
            } else {
              return false 
            }
            return user
          })
          .catch(err => {
            return 'error: ' + err
          })
      
    }
    })
    server.route({
      method: 'GET',
      path: '/FindLeads',
      handler: (req, h) => {
        return Basic.findAll({})
          .then(user => {
            console.log(user);
            if (user) {
            
                return  JSON.stringify(user);
              
            } else {
              return { error: 'User does not exist' }
            }
          })
          .catch(err => {
            return { error: err }
          })
      }
    })
    server.route({
      method: 'GET',
      path: '/Find_Id/{appl_id}',
      handler: async(req, h) => {
        console.log(req.params.appl_id);
        return Basic.findOne({
          where:{
            appl_id:req.params.appl_id
        }
      })
          .then(user => {
            // console.log(user);
            if (user) {
            
                return  JSON.stringify(user);
              
            } else {
              return { error: 'User does not exist' }
            }
          })
          .catch(err => {
            return { error: err }
          })
      }
    })
    server.route({
      method: 'PUT',
      path: '/Updatelead',
      handler: (req, h) => {
        // console.log(req.payload[0].Name,req.payload[1].id,req.payload[1].first_name);
        console.log(req.payload.user[0].Name+''+req.payload.executive[0].id)
        let customer = {Allocated:true,
          Allocatedexecutiveid:req.payload.executive[0].id,
          Allocatedexecutivename:req.payload.executive[0].first_name
        }
        return Basic.findOne({
          where:{
            email:req.payload.user[0].Email
          }
        })
          .then(user => {
            Basic.update(customer,{
              where:{email:req.payload.user[0].Email}
            })
            console.log(user);
            if (user) {
            
                return  JSON.stringify(user);
              
            } else {
              return { error: 'User does not exist' }
            }
          })
          .catch(err => {
            return { error: err }
          })
      }
    })
    server.route({
      method: 'PUT',
      path: '/UpdateStatus',
      handler: async(req, h) => {
        console.log("Hello");
        console.log(req.payload);
        // console.log(req.payload.user[0].Name+''+req.payload.executive[0].id)
        let customer = {aadhar_card:req.payload.aadhar_card,
          pan_card:req.payload.pan_card,
          gst:req.payload.gst,
          brd:req.payload.brd
        }
        return Basic.findOne({
          where:{
            appl_id:req.payload.CName
          }
        })
          .then(user => {
            Basic.update(customer,{
              where:{appl_id:req.payload.CName}
            })
            console.log(user);
            if (user) {
                return  JSON.stringify(user);
              
            } else {
              return { error: 'User does not exist' }
            }
          })
          .catch(err => {
            return { error: err }
          })
      }
    })
  
  },
  name: 'basiclinks'
}