'use strict'
const jwt = require('jsonwebtoken')
const env = require('../database/env.js');
const bcrypt = require('bcrypt')
const fs = require("fs");
const User = require('../models/User')
const nodemailer = require('nodemailer');
const Hapi = require('hapi');
var uploads_directory = "/uploads/"
process.env.SECRET_KEY = 'secret'

var a = [1,2,3,4,5,6,7];
function shuffle(a) {
  var j, x, i;
  for (i = a.length - 1; i > 0; i--) {
      j = Math.floor(Math.random() * (i + 1));
      x = a[i];
      a[i] = a[j];
      a[j] = x;
  }
  return a;
  

}
var check = []
var i,random="";

function arrange(a){
  var c = shuffle(a);
for (i = c.length - 1; i > 0; i--) {
random+=c[i] +""
}
if(check.indexOf(random) !== -1){
  console.log("Value exists!")
  
  } else{
  console.log("Value does not exists!")
  check.push(random);
  } 
  return random;
}

var registration_num = arrange(a);
console.log(registration_num);
exports.plugin = {
  register: (server, options, next) => {
    server.route({
      method: 'POST',
      path: '/register',
      handler: (req, h) => {

        const today = new Date()
        const userData = {
          first_name: req.payload.first_name,
          last_name: req.payload.last_name,
          email: req.payload.email,
          password: req.payload.password,
          role:req.payload.role,
          created: today
        }
        console.log(userData);
        return User.findOne({
          where: {
            // 
            email: req.payload.email
          }
          
        })
        
          .then(user => {
            console.log(user+"abc");
            if (!user) {
              bcrypt.hash(req.payload.password, 10, (err, hash) => {
                userData.password = hash
                console.log(userData.created);
                 User.create(userData)
                  .then(res => {
                    
                    console.log(res+'aa');
                    return { status: res.email + ' Registered!' }
                  })
                  .catch(err => {
                    console.log(err);
                    return 'error: ' + err
                  })
              })
            } else {
              return { error: 'User already exists' }
            }
            return user
          })
          .catch(err => {
            return 'error: ' + err
          })
      }
    })

    server.route({
      method: 'POST',
      path: '/login',
      handler: (req, h) => {
        console.log(req.payload)
        return User.findOne({
          where: {
            email: req.payload.email
          }
        })
          .then(user => {
            console.log(user);
            if (user) {
              if (bcrypt.compareSync(req.payload.password, user.password)) {
                let token = jwt.sign(user.dataValues, process.env.SECRET_KEY, {
                  expiresIn: 1440
                })
                console.log("hello")
                return { token: token };
              } else {
                return {error:"no user"};
              }
            } else {
              return { error: 'User does not exist' }
            }
          })
          .catch(err => {
            return { error: err }
          })
      }
    })

    server.route({
      method: 'GET',
      path: '/profile',
      handler: (req, h) => {
        var decoded = jwt.verify(
          req.headers['authorization'],
          process.env.SECRET_KEY
        )

        return User.findOne({
          where: {
            id: decoded.id
          }
          
        })
          .then(user => {
            if (user) {
              // console.log(decoded.id+"abcde "+JSON.stringify(decoded));
              return user
            } else {
              return 'User does not exist'
            }
          })
          .catch(err => {
            return 'error: ' + err
          })
      }
    })
    server.route({
      method: 'GET',
      path: '/FindMany',
      handler: async (req, h) => {
        return User.findAll({})
          .then(user => {
            console.log(user);
            if (user) {
            
                return  JSON.stringify(user);
              
            } else {
              return { error: 'User does not exist' }
            }
          })
          .catch(err => {
            return { error: err }
          })
      }
    })
    
    server.route({
      method: 'POST',
      path: '/delete_one',
      handler:async  (req, h) => {
        	const id = req.payload.id;

        console.log(req.payload.id);
        return User.destroy({
          where: {
            id:id
          }
        }).then(() => {
          	 return{status:'deleted successfully a customer with id = ' + id};
          	})
        
            
            // User.delete(user)

          .catch(err => {
            return { error: err }
          })
      }
    })
    server.route({
      method: 'GET',
      path: '/getleads',
      handler:async  (req, h) => {
        return User.findAll({
          where: {
            role: "admin-executive"
          }
        })
          .then(user => {
            console.log(user);
            return user;
          })
          .catch(error=>{
            console.log(error);
            return {error:error};

          })
      }
    })
    // server.route({
    //   method: 'POST',
    //   path: '/upload',
    //   config: {
    //     payload: {
    //         output: "stream",
    //         parse: true,
    //         allow: "multipart/form-data",
    //         maxBytes: 2 * 1000 * 1000
    //     }
    // },
    // handler: (request, response) => {
    //   console.log(request.payload);
    //     var result = [];
    //     for(var i = 0; i < request.payload["file"].length; i++) {
    //         result.push(request.payload["file"][i].hapi);
    //         request.payload["file"][i].pipe(fs.createWriteStream(__dirname + "/uploads/" + request.payload["file"][i].hapi.filename))
    //     }
    //     response(result);
    // }
    // })
    server.route({
      method: 'POST',
      path: '/upload',
    
    handler: (request, h) => {
        console.log(request.payload);
        var result = [];
        // for(var i = 0; i < request.payload["file"].length; i++) {
            result.push(request.payload["file"]);
            console.log(result);

            (fs.createWriteStream(__dirname + "/uploads/" + request.payload["file"].filename))
        // }
// (fs.createWriteStream(__dirname + "/uploads/" + request.payload["file"].filename))

        // return h.response(request.payload);
        var data = request.payload;
            if( data.file ) {
              console.log(data);
                console.log( "There is a file" );
                
                var f = data.file
                var name = data.file.filename;
                console.log( "Named " + name );
                // fs.ensureDirSync( __dirname + uploads_directory );
                var path = __dirname + uploads_directory + name;


                var file = fs.createWriteStream( path );
                // f.mv('./uploads/'+name,function(err){
                //   if(err) {
                //     return {error:error};

                //   } else {
                //     return {message:"Its Works"}
                //   }
                // })
                
                file.on( 'error', function( err ) {
                    console.error( err );
                } );
                // data.file.on('end', (err) => { 
                  // const ret = {
                      // filename: data.file.filename,
                      // headers: data.file.headers
                  // }
                  // return JSON.stringify(ret);
              // })

            //     data.file.pipe( file );

            //     data.file.on( 'end', function( err ) {
            //         reply( path );
            //     } );

            // } else {
            //     console.log( "There is no file" );
            //     reply( "None" );
            // }

        }
    return request.payload
    }
    })
    server.route({
      method: 'POST',
      path: '/getexecutive',
      handler:async  (req, h) => {
        console.log(req.payload);
        return User.findOne({
          where: {
            id:req.payload
          }
        })
          .then(user => {
            console.log(user);
            return user;
          })
          .catch(error=>{
            console.log(error);
            return {error:error};

          })
      }
    })

  },
  name: 'users'
}