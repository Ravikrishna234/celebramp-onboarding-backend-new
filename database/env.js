const env = {
  database: 'authentication_project',
  username: 'root',
  password: 'root',
  host: 'localhost',
  dialect: 'mysql',
  email:"",
  pass:"",
  service: 'gmail',
  pool: {
	  max: 5,
	  min: 0,
	  acquire: 30000,
	  idle: 10000
  }
};

module.exports = env;